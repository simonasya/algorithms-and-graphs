#include <vector>
#include <iostream>

using namespace std;

/**
 * \brief reprezentation of graph verticle
 * \details includes vector of edges
 */

class Verticle{
public:
	std::vector<int> edge;
};

/**
 * \brief creates orientations for unoriented graph if possible
 * \details conditions are that there must be way from and to every vertex
 */

class Road{
public:
	void load();
	void print() const;
	void printResult();
	void dfsInit();
	void DFS(int v, int came);
private:
	vector<Verticle> vertex;
	vector<Verticle> verticle;
	short status[100001];
	int low[100001];
	vector<int> in;

	int lastIn;
	int vertexCount;
	int steps;
	int resSize, edgeCount;
	bool descreet, bridge;
};