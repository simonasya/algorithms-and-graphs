#include "railway.h"

/**
 * \brief compare function
 * \return smaller of the argument values
 */

int choose(int x, int y){
	if(x > y)
		return y;
	return x;
}

/**
 * \brief loads graph given as a list of vertexes and edges in standart input
 */

void Road::load(){
	descreet = false;
	int vCount, tmp;
	cin >> vCount;
	vertexCount = vCount;
	edgeCount = 0;
	vertex.resize(vCount);

	for(int i = 0; i < vCount; i++){
		int eCount;
		cin >> eCount;
		if(eCount == 0 || eCount == 1) {
			descreet = true;
			return;
		}
		vertex[i].edge.resize(eCount);
		for(int j = 0; j < eCount; j++){
			cin >> tmp;
			vertex[i].edge[j] = tmp;
			edgeCount++;
		}
	}
	dfsInit();
}

/**
 * \brief prints graph
 */

void Road::print() const{
	for(unsigned int i = 0; i < vertex.size(); i++)
		for(unsigned int j = 0; j < vertex[i].edge.size(); j++)
			cout << i << " " << vertex[i].edge[j] << endl;
}

/**
 * \brief prints final graph with given orientations
 */

void Road::printResult(){
	if(resSize != (edgeCount / 2) || lastIn != 1 || descreet || bridge){
		cout << "No solution." << endl;
		return;
	}

	if(resSize == 0 && vertex.size() == 1){
		cout << 0 << endl;
		return;
	}
	
	for(unsigned int i = 0; i < verticle.size(); i++){
		cout << verticle[i].edge.size();
		for(unsigned int j = 0; j < verticle[i].edge.size(); j++)
			cout << " " << verticle[i].edge[j];
		cout << endl;
	}
}

/**
 * \brief initializes DFS algorithm
 */

void Road::dfsInit(){
	steps = 0;
	bridge = false;
	verticle.resize(vertexCount);
	in.resize(vertexCount);
	resSize = 0;
	lastIn = 0;
	DFS(0, -1);
}

/**
 * \brief DFS algorithm with bridges alocation
 * \details defines orientation of all the edges so there is way from and to every edge
 * 
 * \param v index of current verticle
 * \param came ancestor of current verticle
 */

void Road::DFS(int v, int came){
	status[v] = 1;
	steps++;
	low[v] = 100000;
	in[v] = steps;

	for(unsigned int i = 0; i < vertex[v].edge.size(); i++){ 
		int w = vertex[v].edge[i];
		if(w == came){
			if((i + 1) < vertex[v].edge.size()){
				i++;
				w = vertex[v].edge[i];
			}
			else break;
		}
		if(status[w] == 0 || status[w] == 1){
			if(status[w] == 1 && (lastIn > in[w] || lastIn == 0))
				lastIn = in[w];
			verticle[v].edge.push_back(w);
			resSize ++;
		}
		if(status[w] == 0){
			DFS(w, v);
			if(low[w] >= in[w] && in[w] != 0)
				bridge = true;
			low[v] = choose(low[v], low[w]);
		}
		else {
			if(in[w] < in[v] - 1 && in[w] != 0)
				low[v] = choose(low[v], in[w]);
		}
	}
	status[v] = 2;
	steps++;
}

int main(void){
	Road road;
	road.load();
	road.printResult();
	return 0;
}