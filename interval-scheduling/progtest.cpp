#include "progtest.h"

bool Progtest::loadIntervals(){
	Interval interval;
	int64_t count;
	cin >> count;
	intervalList.resize(count);
	for(int64_t i = 0; i < count; i++){
		cin >> interval.start >> interval.end >> interval.points;
		interval.index = i;
		intervalList[i] = interval;
	}	
	return true;
}

int64_t Progtest::getHighestPoints(){
	int64_t included;
	int64_t tmp;
	intervalList.sort();
	last = -1;

	solutions.resize(intervalList.size());
	solutions[0] = intervalList[0].points;
	result.resize(intervalList.size());
	added.resize(intervalList.size());
	result[0] = -1;
	last = 0;
	added[0] = true;

	for(int64_t i = 1; i < intervalList.size(); i++){
		included = intervalList[i].points;
		tmp = search(i);
		result[i] = tmp;

		if(tmp != -1){
			included = included + solutions[tmp];
		}
		if(included > solutions[i - 1]){
			solutions[i] = included;
			added[i] = true;
			last = i;
		}
		else {
			added[i] = false;
			solutions[i] = solutions[i - 1];
		}
	}
	return solutions[solutions.size() - 1];
}


void Progtest::printSolution(){
	Array<int64_t> final;
	final.resize(result.size());
	int64_t i = last;
	int64_t ind = 0;

	while(i >= 0){
		if(added[i]){
			final[ind] = intervalList[i].index;
			i = result[i];
			ind++;
		}
		else {
			i = i - 1;	
		}
	}
	heapSort(final.getArr(), ind);

	for(int64_t i = 0; i < ind; i++){
		cout << final[i] << " ";
	}	
	cout << endl;
}

int64_t Progtest::search(int64_t index){
	int64_t min = 0;
	int64_t max = index - 1;
	int64_t middle;

	while(min <= max){
		middle = (min + max) / 2;
		if(intervalList[middle].end <= intervalList[index].start){
			if(intervalList[middle + 1].end <= intervalList[index].start)
				min = middle + 1;
			else return middle;
		}
		else
			max = middle - 1;
	}
	return -1;
}
