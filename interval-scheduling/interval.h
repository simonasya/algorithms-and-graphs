#include <iostream>
using namespace std;

class Interval{
public:
	int64_t index;
	int64_t start, end;
	int64_t points;
	bool operator > (const Interval & second){
		if(end > second.end)
			return true;
		return false;
	}
};