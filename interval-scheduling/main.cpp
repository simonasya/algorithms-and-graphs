#include "progtest.h"

int main (void){
	Progtest progtest;
	if(!progtest.loadIntervals())
		return 1;
	cout << progtest.getHighestPoints() << endl;
	progtest.printSolution();
	return 0;
}