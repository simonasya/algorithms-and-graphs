#ifndef PROGTEST 
#define PROGTEST

#include <iostream>
using namespace std;

#include "heapsort.h"
#include "interval.h"
#include "array.h"

class Progtest{
public:
	Array<Interval> intervalList;
	Array<int64_t> solutions;
	Array<int64_t> result;
	Array<bool> added;
	int64_t last;

	bool loadIntervals();
	int64_t getHighestPoints();
	void printSolution();
private:
	int64_t search(int64_t index);
};

#endif