#include <iostream>
using namespace std;

template <typename T>
void heapify(T arr[], int64_t size, int64_t i){
    int64_t largest = i;
    int64_t left = 2 * i + 1;  
    int64_t right = 2 * i + 2; 
 
    if (left < size && arr[left] > arr[largest])
        largest = left;
 
    if (right < size && arr[right] > arr[largest])
        largest = right;
 
    if (largest != i){
        swap(arr[i], arr[largest]);
        heapify(arr, size, largest);
    }
}
 
template <typename T> 
void heapSort(T arr[], int64_t size){
    for (int64_t i = size / 2 - 1; i >= 0; i--)
        heapify(arr, size, i);
 
    for (int64_t i = size - 1; i >= 0; i--){
        swap(arr[0], arr[i]);
        heapify(arr, i, 0);
    }
}
