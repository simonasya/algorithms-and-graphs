#include <iostream>
using namespace std;

template <class T>
class Array{
public:
	Array();
	Array(int64_t size);
	Array(const Array & second);
	~Array();
	bool empty() const; 
	int64_t size() const;
	void resize(int64_t newSize); 
	Array & operator = (const Array & second); 
	T & operator [] (int64_t index);
	void sort(); //quickSort
	T* getArr()const;
private:
	int64_t arraySize;
	T * arr;
};

template <class T>
Array<T>::Array(){
	arraySize = 0;
	arr = NULL;
}

template <class T>
Array<T>::Array(int64_t size){
	arraySize = 0;
	arr = NULL;
	if(size > 0){
		arr = new T[size];
		arraySize = size;
	}
}

template <class T>
Array<T>::Array(const Array & second){
	*this = second;	
}

template <class T>
Array<T>::~Array(){
	if(arr != NULL)
		delete [] arr;
}

template <class T>
int64_t Array<T>::size()const{
	return arraySize;
}

template <class T>
bool Array<T>::empty() const {
	return arraySize == 0;
}

template <class T>
void Array<T>::resize(int64_t newSize){
	T * tmp = NULL;
	if(newSize > 0)
		tmp = new T[newSize];
	if(arraySize > 0){
		for(int64_t i = 0; i < arraySize; i++){
			if(i < newSize) tmp[i] = arr[i];
		}
	}
	if(arr != NULL)
		delete [] arr;
	arr = tmp;
	arraySize = newSize;
}

template <class T>
Array<T> & Array<T>::operator = (const Array & second){
	arraySize = second.arraySize;
	if(arraySize > 0)
		arr = new T[arraySize];
	for(int64_t i = 0; i < arraySize; i++){
		arr[i] = second.getArr()[i];
	}
	return *this;
}

template <class T>
T & Array<T>::operator [] (long int index) {
	int64_t tmp = arraySize;
	if(index >= tmp) resize(index + 1);
	return arr[index];
}

template <class T>
void Array<T>::sort(){
	if(arraySize > 0){
		heapSort(arr, arraySize);
	}
}

template <class T>
T * Array<T>::getArr() const{
	return arr;
}
